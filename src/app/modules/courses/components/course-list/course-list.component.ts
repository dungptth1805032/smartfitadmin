import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CourseDTO } from '@app/models/CourseDTO';
import { DashboardService } from '../../services/dashboard.service';

@Component({
    selector: 'sb-course-list',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './course-list.component.html',
    styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {

    courses?: CourseDTO[];

    constructor(
        private dashboardService: DashboardService,
        private cdr: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.dashboardService.allCourses().subscribe(data => {
            this.courses = data
            this.cdr.detectChanges();
        })
    }
}
