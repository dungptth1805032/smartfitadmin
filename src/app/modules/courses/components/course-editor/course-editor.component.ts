import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-course-editor',
    templateUrl: './course-editor.component.html',
    styleUrls: ['./course-editor.component.scss']
})
export class CourseEditorComponent implements OnInit {

    constructor() { }

    ngOnInit(): void {
    }
}