import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { CountInfoDTO } from '@app/models/CountInfoDTO';
import { AnalyticDTO } from '@app/models/AnalyticDTO';
import { CourseDTO } from '@app/models/CourseDTO';

@Injectable()
export class DashboardService {
    private baseUrl = "https://smartfitapi2.herokuapp.com/api/v2"

    constructor(private http: HttpClient) { }

    get<O>(relativePath: String): Observable<O> {
        return this.http.get<O>(`${this.baseUrl}/${relativePath}`);
    }

    post<I, O>(relativePath: String, postData: I): Observable<O> {
        return this.http.post<O>(`${this.baseUrl}/${relativePath}`, postData)
    }
    countInfo(): Observable<CountInfoDTO> {
        return this.get<CountInfoDTO>("admin-analytic/count-info")
    }

    analytic(): Observable<AnalyticDTO> {
        return this.get<AnalyticDTO>("admin-analytic/analytic")
    }

    allCourses(): Observable<CourseDTO[]> {
        return this.get<CourseDTO[]>("admin/all-course")
    }
}