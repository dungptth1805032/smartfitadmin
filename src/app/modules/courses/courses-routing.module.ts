import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SBRouteData } from '../navigation/models';

import { CoursesModule } from './courses.module';

import { CoursesComponent } from './container/courses/courses.component';

export const ROUTES: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
    },
    {
        path: 'list',
        canActivate: [],
        component: CoursesComponent,
        data: {
            title: 'Courses - SmartFit Admin',
            breadcrumbs: [
                {
                    text: 'Courses',
                    active: true,
                },
            ],
        } as SBRouteData,
    }
];

@NgModule({
    imports: [CoursesModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class CoursesRoutingModule { }