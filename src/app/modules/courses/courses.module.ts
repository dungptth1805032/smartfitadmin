import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AppCommonModule } from './../app-common/app-common.module';
import { NavigationModule } from './../navigation/navigation.module';
import { ChartModule } from 'primeng/chart';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext'

import { CoursesComponent } from './container/courses/courses.component';
import { CourseEditorComponent } from './components/course-editor/course-editor.component';
import { CourseListComponent } from './components/course-list/course-list.component';

import * as dashboardServices from './services';

@NgModule({
    declarations: [
        CoursesComponent,
        CourseEditorComponent,
        CourseListComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        AppCommonModule,
        NavigationModule,
        ChartModule,
        TableModule,
        InputTextModule
    ],
    exports: [
        CoursesComponent,
        CourseEditorComponent
    ],
    providers: [...dashboardServices.services]
})
export class CoursesModule { }