import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppCommonModule } from './../app-common/app-common.module';
import { NavigationModule } from './../navigation/navigation.module';
import { ChartModule } from 'primeng/chart';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext'

import { StaffsComponent } from './container/staffs/staffs.component';
import { StaffsListComponent } from './components/staffs-list/staffs-list.component';

import * as dashboardServices from './services';
@NgModule({
    declarations: [StaffsComponent, StaffsListComponent],
    imports: [
        CommonModule,
        AppCommonModule,
        NavigationModule,
        ChartModule,
        TableModule,
        InputTextModule
    ],
    providers: [...dashboardServices.services]
})
export class StaffsModule { }
