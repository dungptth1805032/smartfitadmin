import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { StaffProfileDTO } from '@app/models/StaffProfileDTO';

@Injectable()
export class DashboardService {
    private baseUrl = "https://smartfitapi2.herokuapp.com/api/v2"

    constructor(private http: HttpClient) { }

    get<O>(relativePath: String): Observable<O> {
        return this.http.get<O>(`${this.baseUrl}/${relativePath}`);
    }

    post<I, O>(relativePath: String, postData: I): Observable<O> {
        return this.http.post<O>(`${this.baseUrl}/${relativePath}`, postData)
    }

    allStaffs(): Observable<StaffProfileDTO[]> {
        return this.get<StaffProfileDTO[]>("admin/all-staff-profile")
    }
}