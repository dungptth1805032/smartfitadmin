import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SBRouteData } from '../navigation/models';
import { StaffsComponent } from './container/staffs/staffs.component';

import { StaffsModule } from './staffs.module';

export const ROUTES: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
    },
    {
        path: 'list',
        canActivate: [],
        component: StaffsComponent,
        data: {
            title: 'Staffs - SmartFit Admin',
            breadcrumbs: [
                {
                    text: 'Staffs',
                    active: true,
                },
            ],
        } as SBRouteData,
    }
];

@NgModule({
    imports: [StaffsModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class StaffRoutingModule { }