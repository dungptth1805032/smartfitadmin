import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CourseDTO } from '@app/models/CourseDTO';
import { StaffProfileDTO } from '@app/models/StaffProfileDTO';
import { UserProfileDTO } from '@app/models/UserProfileDTO';
import { DashboardService } from '../../services/dashboard.service';


@Component({
    selector: 'sb-staffs-list',
    templateUrl: './staffs-list.component.html',
    styleUrls: ['./staffs-list.component.scss']
})
export class StaffsListComponent implements OnInit {
    staffs?: StaffProfileDTO[];
    constructor(
        private dashboardService: DashboardService,
        private cdr: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.dashboardService.allStaffs().subscribe(data => {
            this.staffs = data
            this.cdr.detectChanges();
        })
    }
}