import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IconsModule } from '../icons/icons.module';

const thirdParty = [IconsModule, NgbModule];

import * as appCommonComponents from './components';

@NgModule({
    imports: [CommonModule, RouterModule, ...thirdParty],
    providers: [],
    declarations: [...appCommonComponents.components],
    exports: [...appCommonComponents.components, ...thirdParty],
})
export class AppCommonModule { }