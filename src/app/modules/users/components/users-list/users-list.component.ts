import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CourseDTO } from '@app/models/CourseDTO';
import { UserProfileDTO } from '@app/models/UserProfileDTO';
import { DashboardService } from '../../services/dashboard.service';

@Component({
    selector: 'sb-users-list',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './users-list.component.html',
    styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {


    users?: UserProfileDTO[];

    constructor(
        private dashboardService: DashboardService,
        private cdr: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.dashboardService.allUsers().subscribe(data => {
            this.users = data
            this.cdr.detectChanges();
        })
    }

    display: boolean = false;

    showDialog() {
        this.display = true;
    }
}
