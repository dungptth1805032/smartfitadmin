import { UsersComponent } from './container/users/users.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SBRouteData } from '../navigation/models';

import { UsersModule } from './users.module';

export const ROUTES: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
    },
    {
        path: 'list',
        canActivate: [],
        component: UsersComponent,
        data: {
            title: 'Users - SmartFit Admin',
            breadcrumbs: [
                {
                    text: 'Users',
                    active: true,
                },
            ],
        } as SBRouteData,
    }
];

@NgModule({
    imports: [UsersModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class UsersRoutingModule { }