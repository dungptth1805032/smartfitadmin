import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppCommonModule } from './../app-common/app-common.module';
import { NavigationModule } from './../navigation/navigation.module';

import { ChartModule } from 'primeng/chart';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext'
import { DialogModule } from 'primeng/dialog';
import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';

import { UsersComponent } from './container/users/users.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersViewComponent } from './components/users-view/users-view.component';

import * as dashboardServices from './services';

@NgModule({
    declarations: [
        UsersComponent,
        UsersListComponent,
        UsersViewComponent
    ],
    imports: [
        CommonModule,
        AppCommonModule,
        NavigationModule,
        ChartModule,
        TableModule,
        InputTextModule,
        DialogModule,
        ToolbarModule,
        ButtonModule
    ],
    providers: [...dashboardServices.services]
})
export class UsersModule { }
