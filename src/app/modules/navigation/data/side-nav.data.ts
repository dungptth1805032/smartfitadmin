import { SideNavItems, SideNavSection } from '../../navigation/models';

export const sideNavSections: SideNavSection[] = [
    {
        text: 'CORE',
        items: ['dashboard'],
    },
    {
        text: 'ADDONS',
        items: ['courses', 'users', 'staffs'],
    },
];

export const sideNavItems: SideNavItems = {
    dashboard: {
        icon: 'tachometer-alt',
        text: 'Dashboard',
        link: '/dashboard',
    },
    courses: {
        icon: 'table',
        text: 'Courses',
        link: '/courses',
    },
    users: {
        icon: 'table',
        text: 'Users',
        link: '/users',
    },
    staffs: {
        icon: 'table',
        text: 'Staffs',
        link: '/staffs',
    }
};