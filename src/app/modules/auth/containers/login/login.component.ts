import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'sb-login',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './login.component.html',
    styleUrls: ['login.component.scss'],
})
export class LoginComponent implements OnInit {
    loginForm = new FormGroup({
        email: new FormControl(''),
        password: new FormControl(''),
    });
    isLogin = false;

    constructor(
        private authService: AuthService,
        private router: Router
    ) { }

    ngOnInit() { }

    onSubmit() {
        const formValue = this.loginForm.value
        if (formValue.email != "" && formValue.password != "") {
            this.isLogin = true;
            this.authService.signIn(formValue.email, formValue.password).subscribe(
                (data) => {
                    localStorage.setItem("staffAccess", JSON.stringify(data))
                    this.router.navigate(['dashboard'])
                },
                (error) => {
                }
            )
        }
    }
}