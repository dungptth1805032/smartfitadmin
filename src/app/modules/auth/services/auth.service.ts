import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { StaffAccessDTO } from '@app/models/StaffAccessDTO';
import { StaffSignInDTO } from '@app/models/StaffSignInDTO';

@Injectable()
export class AuthService {
    private baseUrl = "https://smartfitapi2.herokuapp.com/api/v2"

    constructor(private http: HttpClient) { }

    get<O>(relativePath: String): Observable<O> {
        return this.http.get<O>(`${this.baseUrl}/${relativePath}`);
    }

    post<I, O>(relativePath: String, postData: I): Observable<O> {
        return this.http.post<O>(`${this.baseUrl}/${relativePath}`, postData)
    }

    signIn(email: string, password: string): Observable<StaffAccessDTO> {
        const postData = new StaffSignInDTO()
        postData.email = email
        postData.password = password;
        return this.post<StaffSignInDTO, StaffAccessDTO>("staff-auth/sign-in", postData)
    }
}