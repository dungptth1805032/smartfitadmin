import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppCommonModule } from '../app-common/app-common.module';
import { NavigationModule } from '../navigation/navigation.module';
import { ProgressBarModule } from 'primeng/progressbar';

import * as authContainers from './containers';
import * as authService from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
        ProgressBarModule
    ],
    providers: [...authService.services],
    declarations: [...authContainers.containers],
    exports: [...authContainers.containers],
})
export class AuthModule { }