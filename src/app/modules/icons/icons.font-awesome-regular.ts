import { faBell } from '@fortawesome/free-regular-svg-icons';

export const fontAwesomeRegularIcons = {
    faBell,
};