import {
    faFacebook,
    faFacebookF,
    faGithub,
    faGoogle,
    faTwitter,
} from '@fortawesome/free-brands-svg-icons';

export const fontAwesomeBrandsIcons = {
    faGithub,
    faFacebook,
    faFacebookF,
    faGoogle,
    faTwitter,
};