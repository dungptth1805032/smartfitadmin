import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CourseDTO } from '@app/models/CourseDTO';
import { DashboardService } from '../../services/dashboard.service';

@Component({
    selector: 'sb-dashboard-tables',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './dashboard-tables.component.html',
    styleUrls: ['dashboard-tables.component.scss'],
})
export class DashboardTablesComponent implements OnInit {
    courses?: CourseDTO[];

    constructor(
        private dashboardService: DashboardService,
        private cdr: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.dashboardService.allCourses().subscribe(data => {
            this.courses = data
            this.cdr.detectChanges();
        })
    }
}
