import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';

@Component({
    selector: 'sb-dashboard-cards',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './dashboard-cards.component.html',
    styleUrls: ['dashboard-cards.component.scss'],
})
export class DashboardCardsComponent implements OnInit {
    totalCourses = 0;
    totalStaffs = 0;
    totalUsers = 0;
    totalActiveUsers = 0;

    constructor(
        private dashboardService: DashboardService,
        private cdr: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.dashboardService.countInfo().subscribe(data => {
            this.totalCourses = data.totalCourse;
            this.totalStaffs = data.totalStaff;
            this.totalUsers = data.totalUser;
            this.totalActiveUsers = data.totalActiveUser;
            this.cdr.detectChanges();
        })
    }
}