import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';

@Component({
    selector: 'sb-dashboard-charts',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './dashboard-charts.component.html',
    styleUrls: ['dashboard-charts.component.scss'],
})
export class DashboardChartsComponent implements OnInit {
    dataLine: any;
    dataRada: any;

    constructor(
        private dashboardService: DashboardService,
        private cdr: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.dashboardService.analytic().subscribe(data => {
            this.dataLine = {
                labels: [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                ],
                datasets: [
                    {
                        label: 'Monthly Income',
                        data: data.monthlyIncome.datasets[0].data,
                        fill: true,
                        borderColor: '#4BC0C0'
                    }
                ]
            };

            this.dataRada = {
                labels: data.userWorkoutTrend.labels,
                datasets: [
                    {
                        label: 'User Workout Trend',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        pointBackgroundColor: 'rgba(255,99,132,1)',
                        pointBorderColor: '#fff',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: 'rgba(255,99,132,1)',
                        data: data.userWorkoutTrend.datasets[0].data
                    }
                ]
            };
            this.cdr.detectChanges();
        })
    }
}
