export class CourseDTO {
    courseType!: string;
    description!: string;
    estimatedTime!: number;
    id!: number;
    imageUrl!: string;
    level!: string;
    name!: string;
    orderType!: string;
}