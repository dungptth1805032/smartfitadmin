export class StaffAccessDTO {
    accessToken!: string;
    expiryTime!: Date;
    refreshToken!: string;
}