export class CourseStep {
    description!: string;
    id!: number;
    name!: string;
    pose!: string;
    type!: string;
    videoUrl!: string;
}

export class CourseDetailDTO {
    courseSteps!: CourseStep[];
    courseType!: string;
    description!: string;
    estimatedTime!: number;
    id!: number;
    imageUrl!: string;
    level!: string;
    name!: string;
    orderType!: string;
}