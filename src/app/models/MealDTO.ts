export class MealDTO {
    calories!: number;
    id!: number;
    imageUrl!: string;
    name!: string;
    type!: string;
}