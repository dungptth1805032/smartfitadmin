export class StaffProfileDTO {
    address!: string;
    id!: number;
    imageUrl!: string;
    name!: string;
    role!: string;
}