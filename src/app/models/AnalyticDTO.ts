export class Dataset {
    data!: number[];
    label!: string;
}

export class MonthlyIncome {
    datasets!: Dataset[];
    labels!: string[];
}

export class UserWorkoutTrend {
    datasets!: Dataset[];
    labels!: string[];
}

export class AnalyticDTO {
    monthlyIncome!: MonthlyIncome;
    userWorkoutTrend!: UserWorkoutTrend;
}