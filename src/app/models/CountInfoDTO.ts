export class CountInfoDTO {
    totalActiveUser!: number;
    totalCourse!: number;
    totalStaff!: number;
    totalUser!: number;
}