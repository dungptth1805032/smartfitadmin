export class UserProfileDTO {
    age!: number;
    gender!: boolean;
    goal!: string;
    height!: number;
    imageUrl!: string;
    name!: string;
    weight!: number;
}