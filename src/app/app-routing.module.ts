import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthService } from './services/auth.service';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/dashboard',
    },
    {
        path: 'dashboard',
        loadChildren: () =>
            import('./modules/dashboard/dashboard-routing.module').then(m => m.DashboardRoutingModule),
        canActivate: [AuthService]
    },
    {
        path: 'auth',
        loadChildren: () =>
            import('./modules/auth/auth-routing.module').then(m => m.AuthRoutingModule),
    },
    {
        path: 'courses',
        loadChildren: () =>
            import('./modules/courses/courses-routing.module').then(m => m.CoursesRoutingModule),
        canActivate: [AuthService]
    },
    {
        path: 'users',
        loadChildren: () =>
            import('./modules/users/users-routing.module').then(m => m.UsersRoutingModule),
        canActivate: [AuthService]
    },
    {
        path: 'staffs',
        loadChildren: () =>
            import('./modules/staffs/staffs-routing.module').then(m => m.StaffRoutingModule),
        canActivate: [AuthService]
    },
    {
        path: 'error',
        loadChildren: () =>
            import('./modules/error/error-routing.module').then(m => m.ErrorRoutingModule),
    },
    {
        path: '**',
        pathMatch: 'full',
        loadChildren: () =>
            import('./modules/error/error-routing.module').then(m => m.ErrorRoutingModule),
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule { }